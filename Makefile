ui-install:
	docker container run \
		--rm \
		-v ${PWD}/ts/ui:/usr/local/src \
		-w /usr/local/src \
		node:16 npm install

# make ui-download-pkg pkg=<pkg-name>
ui-download-pkg:
	docker container run \
		--rm \
		-v ${PWD}/ts/ui:/usr/local/src \
		-w /usr/local/src \
		node:16 npm install $(pkg)