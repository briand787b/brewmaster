import { FC } from "react";
import { AddIcon, TimeIcon } from "@chakra-ui/icons";
import {
  AspectRatio,
  Box,
  Flex,
  GridItem,
  Heading,
  HStack,
  IconButton,
  Image,
  SimpleGrid,
  Text,
  useBreakpointValue,
  VStack,
} from "@chakra-ui/react";

export const BrewList: FC = () => {
  const colSpan = useBreakpointValue({ base: 4, lg: 2 });

  return (
    <Flex w="full" h="auto" p={[5, 10, 20]} direction="column">
      <HStack w="full" alignItems="center" marginBottom={[5, 10]}>
        <VStack spacing={0} alignItems="flex-start" w="full">
          <Heading size="md" padding={0}>
            All Brews
          </Heading>
          <Text color="gray" marginTop="0px">
            7 brews active or pending
          </Text>
        </VStack>
        <IconButton
          borderColor="gray.200"
          borderWidth={1}
          aria-label="add a brew"
          bgColor="white"
          color="gray.500"
          icon={<AddIcon />}
        />
      </HStack>
      <SimpleGrid columns={4} columnGap={3} rowGap={0} w="full">
        <GridItem colSpan={colSpan} _hover={{ bg: "gray.100" }}>
          <HStack
            w="full"
            justifyContent="flex-start"
            alignItems="flex-start"
            spacing={4}
            py={4}
            borderTopColor="gray.100"
            borderTopWidth={2}
            borderBottomColor="gray.100"
            borderBottomWidth={2}
          >
            <AspectRatio ratio={1} w={[20, 40]}>
              <Image
                src="https://cdn.shopify.com/s/files/1/2785/6868/products/d07_d2c0e181-4eeb-4cb3-95a7-862d477c893c_large.jpg?v=1582213061"
                alt="box"
                borderRadius="5%"
              />
            </AspectRatio>
            <VStack w="full" alignItems="flex-start">
              <Heading size="sm">Smashing Pumpkin</Heading>
              <Text size="sm" color="gray.600">
                Amber ale with pumpkin flavor and spices
              </Text>
              <SimpleGrid columns={4} columnGap={1} rowGap={3} w="full">
                <GridItem colSpan={[2, 1]}>
                  <Box
                    borderWidth={1}
                    borderColor="cadetblue"
                    borderRadius={10}
                    p={1}
                  >
                    <Heading
                      fontSize="0.5em"
                      color="cadetblue"
                      textAlign="center"
                    >
                      NOT STARTED
                    </Heading>
                  </Box>
                </GridItem>
                <GridItem colSpan={[2, 1]}>
                  <Box
                    borderWidth={1}
                    borderColor="gray.500"
                    borderRadius={10}
                    p={1}
                  >
                    <Heading
                      fontSize="0.5em"
                      textAlign="center"
                      color="gray.500"
                    >
                      PENDING DELIVERY
                    </Heading>
                  </Box>
                </GridItem>
              </SimpleGrid>
            </VStack>
          </HStack>
        </GridItem>
        <GridItem colSpan={colSpan} _hover={{ bg: "gray.100" }}>
          <HStack
            w="full"
            justifyContent="flex-start"
            alignItems="flex-start"
            spacing={4}
            py={4}
            borderTopColor="gray.100"
            borderTopWidth={[0, 2]}
            borderBottomColor="gray.100"
            borderBottomWidth={2}
          >
            <AspectRatio ratio={1} w={[20, 40]}>
              <Image
                src="https://cdn.shopify.com/s/files/1/2785/6868/products/J06_2_55888333-b40f-4d87-94a9-74d77ea5703e_large.jpg?v=1581620281"
                alt="box"
                borderRadius="5%"
              />
            </AspectRatio>
            <VStack w="full" alignItems="flex-start">
              <Heading size="sm">Nut Brown Ale</Heading>
              <Text size="sm" color="gray.600">
                Beginner-friendly amber ale
              </Text>
              <SimpleGrid columns={4} columnGap={1} rowGap={3} w="full">
                <GridItem colSpan={[2, 1]}>
                  <Box
                    borderWidth={1}
                    borderColor="cadetblue"
                    borderRadius={10}
                    p={1}
                  >
                    <Heading
                      fontSize="0.5em"
                      color="cadetblue"
                      textAlign="center"
                    >
                      NOT STARTED
                    </Heading>
                  </Box>
                </GridItem>
              </SimpleGrid>
            </VStack>
          </HStack>
        </GridItem>
        <GridItem colSpan={colSpan} _hover={{ bg: "gray.100" }}>
          <HStack
            w="full"
            justifyContent="flex-start"
            alignItems="flex-start"
            spacing={4}
            py={4}
            borderBottomColor="gray.100"
            borderBottomWidth={2}
          >
            <AspectRatio ratio={1} w={[20, 40]}>
              <Image
                src="https://cdn.shopify.com/s/files/1/2785/6868/products/M11_1_x700.jpg?v=1631197228"
                alt="box"
                borderRadius="5%"
              />
            </AspectRatio>
            <VStack w="full" alignItems="flex-start">
              <HStack justifyContent="flex-start">
                <TimeIcon color="firebrick" />
                <Text color="firebrick">Action required soon</Text>
              </HStack>
              <Heading size="sm">Captain's Ration Export Stout</Heading>
              <Text size="sm" color="gray.600">
                Historical British stout
              </Text>
            </VStack>
          </HStack>
        </GridItem>
      </SimpleGrid>
    </Flex>
  );
};
