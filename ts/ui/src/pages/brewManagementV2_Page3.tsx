import { FC } from "react";
import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Box,
  Button,
  Heading,
  List,
  ListIcon,
  ListItem,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  useDisclosure,
  VStack,
} from "@chakra-ui/react";
import {
  CheckCircleIcon,
  CheckIcon,
  NotAllowedIcon,
  SpinnerIcon,
  TimeIcon,
} from "@chakra-ui/icons";

export const BrewManagementV2Page3: FC = () => {
  const { isOpen, onOpen, onClose } = useDisclosure();

  return (
    <VStack>
      <Text fontSize="sm" py={4} bg="gray.100" px={2} w="100%">
        Block Party Amber Ale
      </Text>
      <Accordion
        width="100%"
        defaultIndex={3}
        marginTop="0px"
        allowToggle
        allowMultiple
      >
        <AccordionItem>
          <h2>
            <AccordionButton>
              <CheckIcon paddingLeft={2} />
              <Box flex="1" textAlign="center">
                Clean Equipment
              </Box>
              <AccordionIcon />
            </AccordionButton>
          </h2>
          <AccordionPanel pb={4} bg="gray.100">
            <List spacing={3}>
              <ListItem>
                <ListIcon as={CheckCircleIcon} color="black.200" />
                Lorem ipsum dolor sit amet, consectetur adipisicing elit
              </ListItem>
              <ListItem>
                <ListIcon as={CheckCircleIcon} color="black.200" />
                Assumenda, quia temporibus eveniet a libero incidunt suscipit
              </ListItem>
              <ListItem>
                <ListIcon as={CheckCircleIcon} color="black.200" />
                Quidem, ipsam illum quis sed voluptatum quae eum fugit earum
              </ListItem>
              {/* You can also use custom icons from react-icons */}
              <ListItem>
                <ListIcon as={CheckCircleIcon} color="black.200" />
                Quidem, ipsam illum quis sed voluptatum quae eum fugit earum
              </ListItem>
            </List>
          </AccordionPanel>
        </AccordionItem>

        <AccordionItem>
          <h2>
            <AccordionButton>
              <CheckIcon paddingLeft={2} />
              <Box flex="1" textAlign="center">
                Brew Wort
              </Box>
              <AccordionIcon />
            </AccordionButton>
          </h2>
          <AccordionPanel pb={4} bg="gray.100">
            <List spacing={3}>
              <ListItem>
                <ListIcon as={CheckCircleIcon} color="black.200" />
                Lorem ipsum dolor sit amet, consectetur adipisicing elit
              </ListItem>
              <ListItem>
                <ListIcon as={CheckCircleIcon} color="black.200" />
                Assumenda, quia temporibus eveniet a libero incidunt suscipit
              </ListItem>
              <ListItem>
                <ListIcon as={CheckCircleIcon} color="black.200" />
                Quidem, ipsam illum quis sed voluptatum quae eum fugit earum
              </ListItem>
              {/* You can also use custom icons from react-icons */}
              <ListItem>
                <ListIcon as={CheckCircleIcon} color="black.200" />
                Quidem, ipsam illum quis sed voluptatum quae eum fugit earum
              </ListItem>
            </List>
          </AccordionPanel>
        </AccordionItem>

        <AccordionItem>
          <h2>
            <AccordionButton>
              <CheckIcon paddingLeft={2} />
              <Box flex="1" textAlign="center">
                Primary Fermentation
              </Box>
              <AccordionIcon />
            </AccordionButton>
          </h2>
          <AccordionPanel pb={4} bg="gray.100">
            <List spacing={3}>
              <ListItem>
                <ListIcon as={CheckCircleIcon} color="black.200" />
                Lorem ipsum dolor sit amet, consectetur adipisicing elit
              </ListItem>
              <ListItem>
                <ListIcon as={CheckCircleIcon} color="black.200" />
                Assumenda, quia temporibus eveniet a libero incidunt suscipit
              </ListItem>
              <ListItem>
                <ListIcon as={CheckCircleIcon} color="black.200" />
                Quidem, ipsam illum quis sed voluptatum quae eum fugit earum
              </ListItem>
              {/* You can also use custom icons from react-icons */}
              <ListItem>
                <ListIcon as={CheckCircleIcon} color="black.200" />
                Quidem, ipsam illum quis sed voluptatum quae eum fugit earum
              </ListItem>
            </List>
          </AccordionPanel>
        </AccordionItem>

        <AccordionItem>
          <h2>
            <AccordionButton>
              <Box flex="1" textAlign="center">
                Secondary Fermentation
              </Box>
              <AccordionIcon />
            </AccordionButton>
          </h2>
          <AccordionPanel pb={4} bg="gray.100">
            <List spacing={3}>
              {/* IDEA: Make each item in the list have an onClick handler that opens a modal with detailed instructions on the step */}
              <ListItem>
                <ListIcon as={CheckCircleIcon} color="black.200" />
                Clean Equipment
              </ListItem>
              <ListItem onClick={onOpen}>
                <ListIcon as={CheckCircleIcon} color="black.200" />
                Transfer to Secondary Fermentor
              </ListItem>
              <ListItem>
                {/* Make the spinner actually spin */}
                <ListIcon as={TimeIcon} color="black.200" />
                Wait Two Weeks
              </ListItem>
            </List>
          </AccordionPanel>
        </AccordionItem>

        <AccordionItem>
          <h2>
            <AccordionButton>
              <Box flex="1" textAlign="center">
                Bottling
              </Box>
              <AccordionIcon />
            </AccordionButton>
          </h2>
          <AccordionPanel pb={4} bg="gray.100">
            <List spacing={3}>
              <ListItem>
                <ListIcon as={NotAllowedIcon} color="gray.400" />
                Lorem ipsum dolor sit amet, consectetur adipisicing elit
              </ListItem>
              <ListItem>
                <ListIcon as={NotAllowedIcon} color="gray.400" />
                Assumenda, quia temporibus eveniet a libero incidunt suscipit
              </ListItem>
              <ListItem>
                <ListIcon as={NotAllowedIcon} color="gray.400" />
                Quidem, ipsam illum quis sed voluptatum quae eum fugit earum
              </ListItem>
              {/* You can also use custom icons from react-icons */}
              <ListItem>
                <ListIcon as={NotAllowedIcon} color="gray.400" />
                Quidem, ipsam illum quis sed voluptatum quae eum fugit earum
              </ListItem>
            </List>
          </AccordionPanel>
        </AccordionItem>
      </Accordion>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Transfer to Secondary Fermentor</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            <p>Step-specific content goes here</p>
          </ModalBody>

          <ModalFooter>
            {/* Conditional display 'Mark Complete' or 'Mark Incomplete' depending on step status*/}
            <Button colorScheme="red" mr={3} onClick={onClose}>
              Uncomplete
            </Button>
            {/* <Button colorScheme="green" mr={3} onClick={onClose}>
              Complete
            </Button> */}
          </ModalFooter>
        </ModalContent>
      </Modal>
    </VStack>
  );
};
