import { FC } from "react";
import {
  ChevronLeftIcon,
  ChevronRightIcon,
  TimeIcon,
  CheckIcon,
} from "@chakra-ui/icons";
import {
  AspectRatio,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionIcon,
  AccordionPanel,
  Box,
  Flex,
  Heading,
  HStack,
  IconButton,
  Image,
  Text,
  VStack,
} from "@chakra-ui/react";

export const BrewManagement: FC = () => {
  return (
    <Flex w="full" h="auto" p={[5, 10, 20]}>
      <VStack>
        <HStack>
          <Heading>Smashing Pumpking</Heading>
          <AspectRatio ration={1} w={[20, 40]}>
            <Image
              src="https://cdn.shopify.com/s/files/1/2785/6868/products/d07_d2c0e181-4eeb-4cb3-95a7-862d477c893c_large.jpg?v=1582213061"
              alt="box"
              borderRadius="5%"
            />
          </AspectRatio>
        </HStack>
        <VStack alignItems="stretch">
          <HStack
            bg="green.200"
            justifyContent="space-between"
            _hover={{ bg: "gray.100" }}
          >
            <Heading>1. Equipment Check</Heading>
            <CheckIcon />
          </HStack>
          <HStack
            bg="green.200"
            justifyContent="space-between"
            _hover={{ bg: "gray.100" }}
          >
            <Heading>2. Prime Yeast</Heading>
            <CheckIcon />
          </HStack>
          <HStack
            bg="green.200"
            justifyContent="space-between"
            _hover={{ bg: "gray.100" }}
          >
            <Heading>3. Clean Equipment</Heading>
            <CheckIcon />
          </HStack>
          <HStack
            bg="yellow.200"
            justifyContent="space-between"
            _hover={{ bg: "gray.100" }}
          >
            <Heading>4. Brew Wort</Heading>
            <CheckIcon />
          </HStack>
          <HStack
            bg="red.200"
            justifyContent="space-between"
            _hover={{ bg: "gray.100" }}
          >
            <Heading>5. Primary Fermentation</Heading>
            <CheckIcon />
          </HStack>
          <HStack
            bg="red.200"
            justifyContent="space-between"
            _hover={{ bg: "gray.100" }}
          >
            <Heading>6. Secondary Fermentation</Heading>
            <CheckIcon />
          </HStack>
        </VStack>
      </VStack>
    </Flex>
  );
};
