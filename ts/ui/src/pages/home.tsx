import { FC } from 'react';
import {
  Avatar,
  AspectRatio,
  Flex,
  Heading,
  HStack,
  Text,
  VStack,
} from '@chakra-ui/react';
import { ChevronRightIcon } from '@chakra-ui/icons';

export const Home: FC = () => {
  return (
    <Flex w="full" h="auto" p={[5, 10, 20]}>
      <VStack w="full">
        <HStack w="full" justifyContent="space-between">
          <Text>Back</Text>
          <Heading size="sm">Home</Heading>
          <Text>Dark Mode</Text>
        </HStack>
        <HStack
          justifySelf="self-start"
          justifyContent="start"
          spacing={5}
          w="full"
          paddingTop={[0, 5, 10]}
        >
          <AspectRatio ratio={1} w={20}>
            <Avatar name="Sam Worthington" src="avatar.jpeg" />
          </AspectRatio>
          <VStack alignItems="flex-start">
            <Heading size="md">Sam Worthington</Heading>
            <Text fontSize="sm" color="gray.500">Edit Profile</Text>
          </VStack>
        </HStack>
        <VStack paddingTop={[2, 5, 10]} w="full" spacing={0}>
          <HStack
            justifyContent="space-between"
            w="full"
            borderTopColor="gray.100"
            borderTopWidth={1}
            py={[2, 5]}
            px={[5, 10]}
          >
            <Text fontSize="lg">Brew management</Text>
            <ChevronRightIcon color="gray.500" />
          </HStack>
          <HStack
            justifyContent="space-between"
            w="full"
            borderTopColor="gray.100"
            borderTopWidth={1}
            py={[2, 5]}
            px={[5, 10]}
          >
            <Text fontSize="lg">Equipment management</Text>
            <ChevronRightIcon color="gray.500" />
          </HStack>
          <HStack
            justifyContent="space-between"
            w="full"
            borderTopColor="gray.100"
            borderTopWidth={1}
            py={[2, 5]}
            px={[5, 10]}
          >
            <Text fontSize="lg">Preferences</Text>
            <ChevronRightIcon color="gray.500" />
          </HStack>
          <HStack
            justifyContent="space-between"
            w="full"
            borderTopColor="gray.100"
            borderTopWidth={1}
            py={[2, 5]}
            px={[5, 10]}
          >
            <Text fontSize="lg">Shop</Text>
            <ChevronRightIcon color="gray.500" />
          </HStack>
          <HStack
            justifyContent="space-between"
            w="full"
            borderTopColor="gray.100"
            borderTopWidth={1}
            py={[2, 5]}
            px={[5, 10]}
          >
            <Text fontSize="lg">Brew history</Text>
            <ChevronRightIcon color="gray.500" />
          </HStack>
          <HStack
            justifyContent="space-between"
            w="full"
            borderTopColor="gray.100"
            borderTopWidth={1}
            py={[2, 5]}
            px={[5, 10]}
          >
            <Text fontSize="lg">Plan a brew</Text>
            <ChevronRightIcon color="gray.500" />
          </HStack>
        </VStack>
      </VStack>
    </Flex>
  );
}