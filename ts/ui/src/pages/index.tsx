import { FC, useState } from "react";
import {
  Box,
  Container,
  Drawer,
  DrawerBody,
  DrawerContent,
  DrawerHeader,
  DrawerOverlay,
  Heading,
  HStack,
  IconButton,
  useDisclosure,
} from "@chakra-ui/react";
import { HamburgerIcon } from "@chakra-ui/icons";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { Home } from "./home";
import { BrewStart } from "./brewStart";
import { BrewList } from "./brewList";
import { BrewManagement } from "./brewManagement";
import { BrewManagementV2 } from "./brewManagementV2";
import { BrewManagementV2Page3 } from "./brewManagementV2_Page3";

export const Pages: FC = () => {
  const [size, setSize] = useState("md")
  const { isOpen, onOpen, onClose } = useDisclosure()

  const handleClick = (newSize: string) => {
    setSize(newSize)
    onOpen()
  }

  return (
    <Container maxW="container.xl" p={[0, 5]}>
      <Router>
        {/*bg="facebook.500"*/}
        <HStack w="100%" p={2} justifyContent="space-between" bg="red.700" >
          <Box />
          <Heading size="lg" color="white">
            BrewMaster
          </Heading>
          <IconButton
            aria-label="menu"
            colorScheme="facebook.500"
            onClick={() => handleClick("sm")}
            icon={<HamburgerIcon color="white" />}
          />
        </HStack>
        <Drawer onClose={onClose} isOpen={isOpen} size={"xs"}>
          <DrawerOverlay />
          <DrawerContent>
            <DrawerHeader>{`${size} drawer contents`}</DrawerHeader>
            <DrawerBody>
              {size === "full"
                ? `You're trapped 😆 , refresh the page to leave or press 'Esc' key.`
                : null}
            </DrawerBody>
          </DrawerContent>
        </Drawer>
        <Switch>
          <Route exact path="/brews/1">
            <BrewManagement />
          </Route>
          <Route exact path="/brews/2">
            <BrewManagementV2 />
          </Route>
          <Route exact path="/brews/3">
            <BrewManagementV2Page3 />
          </Route>
          <Route exact path="/brews/start">
            <BrewStart />
          </Route>
          <Route exact path="/brews">
            <BrewList />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </Router>
    </Container>
  );
};
