import { FC } from 'react';

// BrewStart page is where user will start a brew.
// The process begins with the user selecting a brew kit
// and other required information for managing a beer
// from brew to bottle
export const BrewStart: FC = () => {
  return (
    <div>Start a Brew</div>
  )
};