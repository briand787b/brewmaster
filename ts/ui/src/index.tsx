import React from 'react';
import ReactDOM from 'react-dom';
import { ChakraProvider } from "@chakra-ui/react"

import { Pages } from './pages';


ReactDOM.render(
  <React.StrictMode>
    <ChakraProvider>
      <Pages />
    </ChakraProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
