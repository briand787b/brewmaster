# Event Storming Session Nov 19, 2021

## Ubiquitous Language Terminology
- Brew: The entire beer creation process
- User: Entity interacting with the Brewmaster app
- Brewer: Entity brewing beer
- Recipe Step: Discrete action that furthers the progress of a brew
- Recipe Segment: Grouping of temporally related recipe steps
- Recipe Kit: Collection of consumable items required to complete brew
- Recipe Kit Instance: A physical example of a Recipe Kit
- Consumable: Commodity that is designed to be used up relatively quickly
- Equipment: Any inorganic item used to brew beer
- Retailer: Entity selling equipment or recipe kits
- Note: Private text
- Comment: Public text
- Measurement: Private data

## Domain Events (a domain event is something interesting that has happened in the business)
- Recipe Step Created
- Recipe Step Assigned
- Recipe Step Completed
- Recipe Segment Created
- Recipe Segment Assigned
- Recipe Segment Completed
- Recipe Step Notification Created
- Recipe Segment Notification Created
- Recipe Kit Received
- Recipe Kit Created 
- Recipe Kit Started 
- Recipe Kit Modified
- Recipe Kit Completed
- Recipe Kit Deleted
- Recipe Ingredient Consumed
- Custom Ingredient Created
- Custom Ingredient Modified
- Custom Ingredient Associated with Recipe Step
- Custom Ingredient Consumed
- Timer Started
- Timer Elapsed
- Timer Reset
- Timer Paused
- Equipment Created
- Equipment Added
- Equipment Deleted
- Equipment Consumed
- Note Created
- Note Deleted
- Note Associated with Recipe Kit
- Note Associated with Recipe Segment
- Note Associated with Recipe Step
- Note Dissociated from Recipe Kit
- Note Dissociated from Recipe Segment
- Note Dissociated from Recipe Step
- Comment Created
- Comment Deleted
- Comment Associate with Recipe Kit
- Comment Associated with Recipe Segment
- Comment Associated with Recipe Step
- Comment Dissociated from Recipe Kit
- Comment Dissociated from Recipe Segment
- Comment Dissociated from Recipe Step
- Measurement Created
- Measurement Deleted
- Measurement Associated with Recipe Step
- Measurement Dissociated from Recipe Step

## Timelines
### Retailer-Initiated Timeline
- Recipe Kit Created 
- Recipe Kit Modified
- Equipment Created
### Pre-Brew User-Initiated Timeline
- 