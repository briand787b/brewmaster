# Event Storming Session Nov 19, 2021
This session will focus more on the actual brewing process (compared to yesterday's session).  Whenever available, we will use the terminology as defined from the perspective of the retailer in this session.

## Ubiquitous Language Terminology
- Batch: An instance of an in-progress beer recipe
- Brew: Ambiguous term that will not be used in the ubiquitous language
- Wort: Unfermented beer
- Vessel: Anything that can contain liquids
- Brew Kettle: Vessel to boil wort in
- Consumable: Commodity that is designed to be used up relatively quickly
- Equipment: Any inorganic item used to brew beer
- Drying Rack: Equipment used to store drying bottles
- Burner: Heats water and/or wort
- Immersion Wort Chiller: Apparatus that uses flowing cold water to chill wort
- Racking Cane: Transfers beer from one carboy to another
- Siphon Hose: Tubing to transfer beer
- Beer Recipe Kit: All the fresh ingredients you need to brew great beer at home, all in one box. 
- Beer Bottle Cap: Cap to seal beer bottle
- Carboy: Fermentation vessel typically made of plastic or glass
- Airlock: Allows esacpe of CO2 during fermentation, but prevents O2 from entering vessel
- Stopper: Physically seals carboy
- Drilled Stopper: Seals carboy and holds airlock in place
- Solid Stopper: Seals carboy without airlock; useful while moving carboy
- Carboy Harness: Plastic webbing that facilitates movement of carboy
- Carboy Handle: Rubberized metal handle that facilitates movement of empty carboy
- Carboy Dryer: Vented plastic stand holds an inverted carboy steady to let it dry
- Funnel: Facilitates transfer of fluid from one vessel to another
- Thermometer: Device that measures temperature
- Relative Density: The ratio of the density of a substance to the density of a given reference material
- Specific Gravity: Relative density where the reference material is water
- Hydrometer: Instrument used for measuring the relative density of liquids based on the concept of buoyancy
- pH: Measure of the acidity of a solution
- Refractomometer: Measures the sugar content of a solution via the refraction of light
- Brix: Degrees Brix (symbol °Bx) is the sugar content of an aqueous solution
- Fermentation: The chemical breakdown of a substance by bacteria, yeasts, or other microorganisms, typically involving effervescence and the giving off of heat.
- Yeast: Converts carbohydrates to carbon dioxide and alcohols through the process of fermentation
- Dry Yeast: Yeast delivered as a dry powder
- Liquid Yeast: Yeast delivered as a liquid solution
- Yeast Cell Count: Number of yeast cells in a given volume
- Yeast Starter: Small volumes of malt sugar solutions given to the yeast that allows it to eat, reproduce and build energy reserves for the fermentation process.
- Aeration: Adding oxygen to wort
- Brush: Bristled tool to physically remove particles from equipment
- Cleaner: Removes grime and particulates from surface of equipment
- Cleanser: Synonym of cleaner
- Sanitizer: Chemical used to disinfect physically clean equipment
- PBW: Powdered Brewery Wash is an alkali cleaner
- Star San: Acid based sanitizer
- Contact Time: Duration
- Hops: Provide both bitterness (IBUs) and a wide variety of flavor to numerous beer styles
- Bottling Bucket: Bucket of beer and priming solution to be transferred to bottles

## Domain Events
- Cleaning Solution Created
- Cleaning Solution Applied to Equipment
- Measured Water Volume Collected
- Measure Water Volume Transferred to Brew Kettle
- Burner Lit
- Prepare Grain 
- Add Grain to Heating Water
- Timer Started
- Maximum Wort Temperature Reached
- Timer Elapsed
- Wort Stirred
- Wort Started Boiling
- Wort Boiled Over
- Burner Turned Off
- Liquid Malt Added to Wort
- Dried Malt Extract Added to Wort
- Hops Added to Wort
- Equipment Sanitized
- Wort Chilled
- Water Added to Carboy
- Wort Added to Carboy
- Primary Fermentator Wort Volume Adjusted
- Wort Aerated
- Yeast Added to Primary Fermentor
- Airlock Attached to Primary Fermentor 
- Primary Fermentor Put into Storage
- Temperature Measured
- Specific Gravity Measured
- Wort Transferred
- Secondary Fermentor Put into Storage
- Priming Solution Mixed
- Priming Solution Brought to Boil
- Priming Solution Added to Bottling Bucket
- Beer Transferred to Bottling Bucket
- Bottling Solution Stirred
- Bottling Solution Transferred to Bottle
- Bottle Capped
- Equipment Degraded
- Equipment Rinsed
- Equipment Started Drying

## Timeline

### Prep Day
#### Happy Path
1. Cleaning Solution Created
2. Cleaning Solution Applied to Equipment
3. Equipment Rinsed
4. Equipment Started Drying
#### Sad Path - Broken Equipment
1. Cleaning Solution Created
2. Cleaning Solution Applied to Equipment
3. Equipment Rinsed
(Carboy dropped and broken)
4. Equipment Degraded

### Brew Day
#### Happy Path
1. Measured Water Volume Collected
2. Measured Water Volume Transferred to Brew Kettle
3. Burner Lit
4. Brew Kettle Put on Burner
5. Grain Prepared
6. Grain Added to Heating Water
7. Grain Steeped for 30 minutes
8. Grain Bag Removed
9. Water Brought to Boil
10. Brew Kettle Removed from Burner
11. 6 lbs Malt Syrup Added to Water
12. 1 lb Pilsen Light DME Added to Water
13. 1 lb Wheat DME Added to Water
14. 1 lb Lactose Added to Water
15. Wort Brougth to Boil
16. 0.5 oz Warrior Hops Added
17. 60 Minutes Elapsed
18. Wort Rapidly Cooled to 170F
19. Wort Removed from Chiller
20. 2 oz Citra Hops Added
21. 2 oz Ekuanot Hops Added 
22. Hops Steeped for 20 Minutes
23. Wort Rapidly Chilled to 100F
24. Fermenting Equipment & Yeast Packet Sanitized
25. Primary Fermentor Filled with 2 Gallons of Cold Water
26. Cooled Wort Added to Primary Fermentor
27. Wort Aerated
28. Wort's Specific Gravity Recorded
29. Yeast Added to Primary Fermentor
30. 1 tbsp Sanitized Water Added to Airlock
31. Primary Fermentor Sealed with Airlock
32. Primary Fermentor Transferred to Cool, Dark Location