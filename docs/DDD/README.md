# Domain Driven Design

## Bounded Context Communications
### Cooperation
This model of bounded context communication is suitable for intra-project communication.  Specifically, brewmaster will use the shared kernel pattern.  This pattern puts the communication contract inside of a compiled library, the shared kernel.  This is preferable over the partnership model because the contract is more clearly defined in the shared kernel pattern.
### Customer-Supplier
The information on what steps are required will obviously originate from the retailer, but there is ambiguity as to how that information is gathered by Brewmaster.  For example, a web scraper could gather the information directly from the retailer's site.  Or the retailer could submit PDFs directly to Brewmaster.  And, finally, manual entry by Brewmaster staff could be another option.  Manual entry is the most likely path to start with since it requires the least amount of engineering effort.

I believe that the correct desription of the system is that the retailer is the upstream context.  The downstream context is the server and its associated model that the upstream context integrates with.  In the case of an automated data miner, the anti-corruption layer is the logic I put into the miner to translate the upstream context into something suitable for the downstream context.  In the case of manual data entry, the person doing the data entry is the anti-corruption layer.

## Context Map
TODO: Create a context map like figure 4-7 of the What is Domain Driven Design book at O'Reilly.