# Hypothetical Features

## Readiness Check [quality of life, revenue generator]
A recipe kit could be identified as 'ready' or 'needs extra equipment' based on the equipment the user registered in the app.

## Recipe Mods [social]
If the social aspect of the app is embraced there could be modified recipes forked from retail-provided recipes.  How these would be implemented is unclear, since the modifications can encompass all or one of modifications to the instructions, ingredients, or required equipment.  The treatment of the modified recipes could mirror the treatment of forked projects in GitHub or it could use something entirely different it that model is incompatible with the flow of this app.  I need to ensure that I am not ignoring fundamental knowledge about git forking that would be confusing to users who have never been expose to it before.