# UI Design Notes

## Design for both novice and experienced users
IDEA: Create different brew pathways for different experience levels, similar to the way that video single-player video games can be played on novice, intermediate, veteran, or expert modes.  Some brew kits would only be brewable at certain levels based on their nature.  For example, Block Party Amber Ale would only really have an novice pathway due to it being a simple, single-stage brew.  Something like the Dry Irish Stout would be intermediate only for the extract version, but veteran for the all-grain version.